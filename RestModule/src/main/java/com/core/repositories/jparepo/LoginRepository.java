package com.core.repositories.jparepo;

import com.core.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by cihan-net-bt on 8.04.2016.
 */
@Repository
public interface LoginRepository extends JpaRepository<User,Long>{
    public User findByUserName(String userName);
}
