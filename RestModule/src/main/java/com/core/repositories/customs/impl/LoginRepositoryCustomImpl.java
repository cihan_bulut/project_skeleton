package com.core.repositories.customs.impl;

import com.core.repositories.customs.LoginRepositoryCustom;
import com.core.repositories.jparepo.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by cihan-net-bt on 7.04.2016.
 */
@Repository
public class LoginRepositoryCustomImpl implements LoginRepositoryCustom {

    @Autowired
    private LoginRepository loginRepository;

    @PersistenceContext
    private EntityManager entityManager;

//    @Override
//    public UserInfoService loadUserInfoByUserName(String userName) {
//        UserInfoService userInfo = new UserInfoService();
//        User user = loginRepository.findByUserName(userName);
//        userInfo.setUserName(user.getUserName());
//        userInfo.setPassword(user.getPassword());
//        userInfo.setFirstName(user.getFirstName());
//        userInfo.setLastName(user.getLastName());
//        userInfo.setEmail(user.getEmail());
//
//        return userInfo;
//    }


}
