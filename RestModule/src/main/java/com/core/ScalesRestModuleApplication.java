package com.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

import java.io.IOException;


@SpringBootApplication
@ComponentScan("com.core.*")
@EntityScan("com.core.*")
@EnableJpaRepositories("com.core.repositories.*")
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableAutoConfiguration
public class ScalesRestModuleApplication {

	public static void main(String[] args)throws IOException {
		SpringApplication.run(ScalesRestModuleApplication.class, args);
	}

}
