package com.core.services;

import com.core.models.Right;
import com.core.models.Role;
import com.core.models.User;

import java.util.List;

/**
 * Created by cihan-net-bt on 7.04.2016.
 */
public interface LoginService {

    public User getUserAndRoles(long user_id);

    public void addUser(User user);

    public void addRole(Role role);

    public Role getRole(long id);

    public Right getRight(long id);

    public void addRight(Right right);

    public void assignRolesToUser(User user);

    public void assignRightsToRole(Role role);

    public User findByUserName(String userName);

    public List<User> getAllUsers();

    public User getUserById(Long id);
}
