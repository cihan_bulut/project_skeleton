package com.core.services.impl;

import com.core.models.Right;
import com.core.models.Role;
import com.core.models.User;
import com.core.repositories.customs.LoginRepositoryCustom;
import com.core.repositories.jparepo.LoginRepository;
import com.core.services.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cihan-net-bt on 7.04.2016.
 */
@Service
@Transactional
public class LoginServiceImpl implements LoginService {

    private static final Logger LOG = LoggerFactory.getLogger(LoginServiceImpl.class);

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private LoginRepository loginRepository;
    @Autowired
    private LoginRepositoryCustom loginRepositoryCustom;

    public User getUserAndRoles(long user_id) {
        return loginRepository.findOne(user_id);
    }

    public void addUser(User user) {
        User u = new User();
        u.setFirstName(user.getFirstName());
        u.setLastName(user.getLastName());
        u.setUserName(user.getUserName());
        u.setPassword(new ShaPasswordEncoder(256).encodePassword(user.getPassword(), null));
        u.setEmail(user.getEmail());
        u.setRoles(user.getRoles());

        loginRepository.save(u);
    }

    public void addRight(Right right) {
        entityManager.persist(right);
    }

    public void addRole(Role role) {
        entityManager.persist(role);
    }

    @Override
    public Role getRole(long id) {
        return (Role) entityManager.createQuery("select r from Role r where r.id = :id").setParameter("id", id).getSingleResult();
    }

    @Override
    public Right getRight(long id) {
        return (Right) entityManager.createQuery("select r from Right r where r.id = :id").setParameter("id", id).getSingleResult();
    }

    public void assignRolesToUser(User user) {
        loginRepository.save(user);

    }

    @Override
    public void assignRightsToRole(Role role) {
        entityManager.merge(role);
    }

    @Override
    public User findByUserName(String userName) {
        return loginRepository.findByUserName(userName);
    }

    @Override
    public List<User> getAllUsers() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        List<String> list = new ArrayList<>();

        return entityManager.createQuery(cq).getResultList();
    }

    @Override
    public User getUserById(Long id) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        cq.where(cb.equal(root.get("id"), id));
        return entityManager.createQuery(cq).getSingleResult();
    }
}
