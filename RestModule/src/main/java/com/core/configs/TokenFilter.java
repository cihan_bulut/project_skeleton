package com.core.configs;

import com.core.security.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.Enumeration;

/**
 * Created by cihan-net-bt on 22.04.2016.
 */
@Component
public class TokenFilter extends GenericFilterBean {

    @Autowired
    private TokenManager tokenManager;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpResponse.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        httpResponse.setHeader("Access-Control-Max-Age", "3600");
        httpResponse.setHeader("Access-Control-Allow-Credentials", "false");
        httpResponse.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,X-Auth-Token");
        httpResponse.setHeader("Access-Control-Expose-Headers", "Origin, X-Requested-With, Content-Type, Accept,X-Auth-Token");
        Principal principal = httpRequest.getUserPrincipal();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Enumeration names = httpRequest.getHeaderNames();
        while (names.hasMoreElements()) {
            String name = (String) names.nextElement();
            Enumeration values = httpRequest.getHeaders(name); // support multiple values
            if (values != null) {
                while (values.hasMoreElements()) {
                    String value = (String) values.nextElement();
                    System.out.println(name + ": " + value);
                }
            }
        }

        boolean authenticated = false;
        System.out.println(httpRequest.getHeader("referer"));
//        chain.doFilter(httpRequest, httpResponse);
        if (authentication != null && principal != null && principal.getName() != null) {
            authenticated = tokenManager.authenticate(httpRequest, httpResponse, authentication);
            if (authenticated) {
                logger.info("You are successfully authenticated");
                logger.info(httpRequest.getRequestURL());
                chain.doFilter(httpRequest, httpResponse);
            } else {
                logger.error("Authentication is fail");
                new SecurityContextLogoutHandler().logout(httpRequest, httpResponse, authentication);
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        } else {
            String s = httpRequest.getRequestURL().toString();
            if(s.equals("http://192.168.1.33:8080/kantarfx.jar")){
                System.out.println("allowed");
                chain.doFilter(httpRequest, httpResponse);
            } else {
                logger.error("You are not authenticated!!");
                new SecurityContextLogoutHandler().logout(httpRequest, httpResponse, authentication);
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }



        logger.info(" === AUTHENTICATION: " + authentication);
    }


}
