package com.core.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Created by cihanblt on 8/16/2016.
 */
@Configuration
public class CorsConfig {

        @Bean
        public CorsFilter corsFilter() {
            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
            CorsConfiguration config = new CorsConfiguration();
//            config.setAllowCredentials(true);
            config.addAllowedOrigin("*");
//            config.addAllowedOrigin("http://192.168.1.33:9000");
            config.addAllowedHeader("Authorization");
            config.addAllowedHeader("Content-Type");
            config.addAllowedHeader("X-Auth-Token");

            config.addExposedHeader("Authorization");
            config.addExposedHeader("Content-Type");
            config.addExposedHeader("X-Auth-Token");

            config.addAllowedMethod("OPTIONS");
            config.addAllowedMethod("HEAD");
            config.addAllowedMethod("GET");
            config.addAllowedMethod("PUT");
            config.addAllowedMethod("POST");
            config.addAllowedMethod("DELETE");
            config.addAllowedMethod("PATCH");
            source.registerCorsConfiguration("/**", config);
            return new CorsFilter(source);
        }

}
