package com.core.configs;

import com.core.security.CustomUserDetailsService;
import com.core.security.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by cihan-net-bt on 12.04.2016.
 */
@Configuration
@ComponentScan("com.core.*")
@EnableWebSecurity
@EnableTransactionManagement(proxyTargetClass = true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(new CorsConfig().corsFilter(), ChannelProcessingFilter.class).addFilterAfter(new CorsConfig().corsFilter(),ChannelProcessingFilter.class).httpBasic()
                .and()
                .csrf().disable()
                .authorizeRequests().antMatchers("/login/logout","/login/waiting","/login/add-user","/core.jnlp", "/kantarfx.jar").permitAll()
                .anyRequest().authenticated()
                .and()
        .exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint)
        .and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }



    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setSaltSource(null);
        daoAuthenticationProvider.setPasswordEncoder(new ShaPasswordEncoder(256));
        daoAuthenticationProvider.setUserDetailsService(customUserDetailsService);
        auth.authenticationProvider(daoAuthenticationProvider);
    }


}
