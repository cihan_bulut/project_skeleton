package com.core.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by cihan-net-bt on 18.04.2016.
 */
public class UserAuthority implements GrantedAuthority {
    private String authority;

    public UserAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    @Override
    public int hashCode() {
        return authority.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(!(obj instanceof UserAuthority)) return false;
        return ((UserAuthority)obj).getAuthority().equals(authority);
    }

    @Override
    public String toString() {
        return "UserAuthority{" +
                "authority='" + authority + '\'' +
                '}';
    }
}
