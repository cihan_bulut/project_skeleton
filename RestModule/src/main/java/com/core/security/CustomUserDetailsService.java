package com.core.security;

import com.core.models.User;
import com.core.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by cihan-net-bt on 21.04.2016.
 */
@Component
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private LoginService loginService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = loginService.findByUserName(s);
        UserProfile userProfile = null;
        if(user == null){
            throw new UsernameNotFoundException("username not found");
        }else{
            userProfile = new UserProfile();
            userProfile.setFirstName(user.getFirstName());
            userProfile.setLastName(user.getLastName());
            userProfile.setUserName(user.getUserName());
            userProfile.setPasswordHash(user.getPassword());
            userProfile.setEmail(user.getEmail());
            userProfile.setId(user.getId());
            userProfile.setRoles(user.getRoles());
            userProfile.setAccountNonLocked(true);
            userProfile.setAccountNonExpired(true);
            userProfile.setCredentialsNonExpired(true);
            userProfile.setActive(true);
        }
        return userProfile;
    }
}
