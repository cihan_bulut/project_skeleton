package com.core.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cihan-net-bt on 4/25/2016.
 */
public interface TokenManager {

    public String createToken(UserDetails userDetails);

    public boolean validateToken(String header);

    public boolean authenticate(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,Authentication authentication) throws IOException;

}
