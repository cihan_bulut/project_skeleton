package com.core.security;

import com.core.models.Right;
import com.core.models.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cihan-net-bt on 18.04.2016.
 */
public class UserProfile implements UserDetails {

    private final String ROLE_PREFIX = "ROLE_";
    private final String RIGHT_PREFIX = "ROLE_RIGHT_";

    private Long id;
    private Integer version;
    private String email;
    private String userName;
    private String passwordHash;
    private String firstName;
    private String lastName;
    private String phone;
    private boolean accountNonLocked;
    private boolean accountNonExpired;
    private boolean credentialsNonExpired;
    private boolean isActive;

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    private Set<Role> roles;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean isEnabled() {
        return this.isActive;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public String getUsername() {
        return this.getUserName();
    }

    @Override
    public String getPassword() {
        return passwordHash;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<UserAuthority> authorities = new HashSet<>();
        for (Role role : roles){
            UserAuthority userAuthorityRole = new UserAuthority(ROLE_PREFIX + role.getName() );
            authorities.add(userAuthorityRole);
            for(Right right : role.getRights()){

                UserAuthority userAuthority = new UserAuthority(RIGHT_PREFIX + right.getName());
                authorities.add(userAuthority);
            }

        }
        /*Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));*/
        return authorities;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "phone='" + phone + '\'' +
                ", roles=" + getAuthorities() +
                ", userName='" + userName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isActive=" + isActive +
                ", id=" + id +
                ", firstName='" + firstName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
