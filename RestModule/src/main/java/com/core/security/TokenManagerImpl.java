package com.core.security;

import com.core.generics.GenericService;
import com.core.models.TokenInfo;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by cihan-net-bt on 4/25/2016.
 */
@Component
public class TokenManagerImpl implements TokenManager{

    private final Logger logger = LoggerFactory.getLogger(TokenManagerImpl.class);

    private static final String X_AUTH_TOKEN = "X-Auth-Token";

    private ConcurrentMap<String,UserDetails> tokenMap = new ConcurrentHashMap<String, UserDetails>();

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private GenericService genericService;

    @Override
    public boolean authenticate(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,Authentication authentication) throws IOException{
        String headerToken = httpServletRequest.getHeader(X_AUTH_TOKEN);
        boolean isTokenValid = validateToken(headerToken);
        boolean isTokenExpired = true;
        if (headerToken != null){
            isTokenExpired = isTokenExpired(getTokenTime(headerToken));
        }

        if(isTokenValid && !isTokenExpired && headerToken != null){
            return true;
        }else if (authentication.isAuthenticated() && headerToken == null){
            UserDetails userDetails = customUserDetailsService.loadUserByUsername(authentication.getName());
            String newToken = createToken(userDetails);
            httpServletResponse.setHeader(X_AUTH_TOKEN,newToken);
            return false;
        }else {
            tokenMap.remove(X_AUTH_TOKEN);
            httpServletResponse.setHeader(X_AUTH_TOKEN,null);
            return false;
        }
    }

    @Override
    public String createToken(UserDetails userDetails) {
        long EXPIRATION_TIME = System.currentTimeMillis() + 10000 * 60 * 60; //1 saatlik
        if(userDetails != null) {
            String pattern = userDetails.getUsername() + ":" + EXPIRATION_TIME + ":" + generateToken();
            String tokenn = Hex.encodeHexString(pattern.getBytes());
            tokenMap.put(tokenn,userDetails);
            TokenInfo tokenInfo = new TokenInfo();
            tokenInfo.setToken(tokenn);
            tokenInfo.setUserName(userDetails.getUsername());
            genericService.saveEntity(tokenInfo);
            logger.info("Token : " + tokenn);
            return tokenn;
        }else{
            return null;
        }
    }

    public String generateToken(){
        byte[] bytes = new byte[20];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(bytes);
        return new String(new Md5PasswordEncoder().encodePassword(bytes.toString(),null));
    }

    @Override
    public boolean validateToken(String header) {
        String userFromToken = null;
        if(header == null){
            return false;
        }else{
            userFromToken = getUserFromToken(header);
            UserDetails userDetails = customUserDetailsService.loadUserByUsername(userFromToken);
            if(tokenMap.containsKey(header)){
                if(userDetails != null ){
                    return true;
                }else{
                    return false;
                }

            }else{
                return false;
            }
        }
    }

    private String getTokenTime(String header){
        String decodeToken = getDecodeToken(header);
        return decodeToken.toString().split(":")[1];
    }

    private String getUserFromToken(String header){
        String decodeToken = getDecodeToken(header);
        return decodeToken.toString().split(":")[0];
    }

    private String getDecodeToken(String header){
        byte[] decode = null;
        String decodeToken = null;
        try {
            decode = Hex.decodeHex(header.toCharArray());
            decodeToken = new String(decode, Charset.forName("UTF-8"));
        }catch (Exception e){
            e.printStackTrace();
        }
        return decodeToken;
    }

    private boolean isTokenExpired(String time){
        long now =  System.currentTimeMillis();
        long timeInToken = Long.parseLong(time);
        return timeInToken <= now;
    }

    public String getHashPassword(String password,String salt){
        ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(256);
        return shaPasswordEncoder.encodePassword(password,salt);
    }

}
