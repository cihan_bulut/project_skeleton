package com.core.generics;

/**
 * Created by cihan-net-bt on 4/26/2016.
 */
public interface GenericService {
    public static final int a = 1;
    public abstract  <T> void saveEntity(T t);
}
