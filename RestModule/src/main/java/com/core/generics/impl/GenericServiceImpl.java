package com.core.generics.impl;

import com.core.generics.GenericService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by cihan-net-bt on 4/26/2016.
 */
@Service
@Transactional
public class GenericServiceImpl implements GenericService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public <T> void saveEntity(T t) {
        entityManager.persist(t);
    }
}
