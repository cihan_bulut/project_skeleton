package com.core.interceptors;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Created by cihan-net-bt on 4/28/2016.
 */
public class RequestIntercepter implements Interceptor {
    private final static Logger LOG = Logger.getLogger(RequestIntercepter.class);

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        long t1 = System.nanoTime();
        LOG.info("Sending request ; url = " + request.url() + " , connection = " + chain.connection() + " , headers = " + request.headers() );
        LOG.info("request body" + request.body());

        Response response = chain.proceed(request);
        long t2 = System.nanoTime();
        LOG.info("received response ; url = " +  response.request().url() + "  in " + (t2 - t1) / 1e6d + " response headers = " + response.headers());
        LOG.info(response.body());
        return response;
    }
}
