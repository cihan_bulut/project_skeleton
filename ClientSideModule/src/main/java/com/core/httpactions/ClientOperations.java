package com.core.httpactions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.core.dto.UserInfo;
import okhttp3.*;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by cihan-net-bt on 4/28/2016.
 */
public class ClientOperations {

    private static ClientOperations clientOperations = null;

    private HttpResponse httpResponse = null;

    private static final HttpClient httpClient = HttpClientBuilder.create().build();

    private String credential;

    private String token;

    private final static String AUTHORIZATION_HEADER = "Authorization";

    private final static String X_AUTH_TOKEN_HEADER = "X-Auth-Token";

    private static final Logger LOG = Logger.getLogger(ClientOperations.class);

    private ClientOperations() {

    }

    public static ClientOperations getInstance() {
        if (clientOperations == null) {
            synchronized (ClientOperations.class) {
                return new ClientOperations();
            }
        }
        return clientOperations;
    }

    /*public String getJson(String url) throws IOException {
        String credential = Credentials.basic("ccc", "12345");
        return getContentBody();
    }*/

    public boolean authenticate(String url, String credential) throws IOException {
        this.credential = credential;
        HttpResponse httpResponse = getAuthentication(url);
        String token = getTokenOnAuthentication(httpResponse);
        if (token != null) {
            this.token = token;
            httpResponse = getAuthentication(url, token);
            this.httpResponse = httpResponse;
            return true;
        } else {
            System.out.println("bad username or password");
        }
        return false;
    }

    public HttpResponse getAuthentication(String url) throws IOException {
        return getAuthentication(url, null);
    }

    public HttpResponse getAuthentication(String url, String token) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader(AUTHORIZATION_HEADER, credential);
        if (this.token != null) {
            httpGet.addHeader(X_AUTH_TOKEN_HEADER, this.token);
        }
        HttpResponse httpResponse = httpClient.execute(httpGet);
        return httpResponse;
    }

    public String getTokenOnAuthentication(HttpResponse httpResponse) {
        Header[] header = httpResponse.getHeaders(X_AUTH_TOKEN_HEADER);
        try {
            String token = header[0].getValue();
            return token;
        } catch (ArrayIndexOutOfBoundsException e) {
            LOG.error(e.getMessage());
            return null;
        }
    }

    public String getContentBody() throws IOException {
        return getContentBody(this.httpResponse);
    }

    public String getContentBody(HttpResponse httpResponse) throws IOException {
        if (isAuthenticated()) {
            InputStream inputStream = httpResponse.getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }
            return stringBuffer.toString();
        } else
            return null;
    }

    public int getHttpStatusCode(String url,String argument) throws IOException{
        if (isAuthenticated()) {
            if (argument != null) {
                url += "?" + argument;
            }
            HttpGet httpGet = new HttpGet(url);
            httpGet.addHeader(AUTHORIZATION_HEADER, credential);
            httpGet.addHeader(X_AUTH_TOKEN_HEADER, token);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            return httpResponse.getStatusLine().getStatusCode();
        } else {
            return 0;
        }
    }

    public UserInfo getUserInfo() {
        if (isAuthenticated()) {
            ObjectMapper objectMapper = null;
            UserInfo s = null;
            try {
                objectMapper = new ObjectMapper();
                s = objectMapper.readValue(getContentBody(), UserInfo.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s;
        } else {
            return null;
        }
    }

    public String getContentBodyOnUrl(String url) throws IOException {
        return getContentBodyOnUrl(url, null);
    }

    public String getContentBodyOnUrl(String url, String argument) throws IOException {
        if (isAuthenticated()) {
            if (argument != null) {
                url += "?" + argument;
            }
            HttpGet httpGet = new HttpGet(url);
            httpGet.addHeader(AUTHORIZATION_HEADER, credential);
            httpGet.addHeader(X_AUTH_TOKEN_HEADER, token);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            return getContentBody(httpResponse);
        } else {
            return null;
        }
    }

    public String postJsonData(String url, Object json) throws IOException {
        if (isAuthenticated()) {
            StringEntity stringEntity = new StringEntity((String) json, ContentType.APPLICATION_JSON);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(stringEntity);
            httpPost.addHeader("Authorization", credential);
            httpPost.addHeader(X_AUTH_TOKEN_HEADER, token);
            httpPost.addHeader("Content-Type", "application/json");
            HttpResponse httpResponse = httpClient.execute(httpPost);
            LOG.info("status : " + httpResponse.getStatusLine());
            return getContentBody(httpResponse);
        } else {
            return null;
        }

    }

    public boolean isAuthenticated() {
        String statusCode = String.valueOf(httpResponse.getStatusLine().getStatusCode());
        if (statusCode != null && statusCode.equals("200")) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) throws IOException {
        String credential = Credentials.basic("ccc", "1234");
        ClientOperations clientOperations = ClientOperations.getInstance();
        String path = "http://localhost:8080/login/user-info";
        clientOperations.authenticate(path, credential);
        String s = clientOperations.getContentBody();
        boolean isAuthentitaced = clientOperations.isAuthenticated();
        System.out.println(isAuthentitaced);
        String data = "{ \"vehicleName\" : \"ttt\",\"brand\" : \"ford\",\"model\" : \"xxx\"}";
        String a = clientOperations.postJsonData("http://localhost:8080/main/save-vehicle", data);
        System.out.println(a);
    }
}
