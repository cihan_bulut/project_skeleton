package com.core;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Hello world!
 *
 */
public class App extends Application
{

    public static void main( String[] args )
    {
        launch(App.class);
    }

    public void start (Stage primaryStage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("/fxmls/login.fxml"));
        setUserAgentStylesheet(STYLESHEET_MODENA);
        Scene scene = new Scene(parent);
//        ResponsiveHandler.addResponsiveToWindow(primaryStage);
        primaryStage.setTitle("Giriş");
        primaryStage.setScene(scene);
        primaryStage.show();

    }


}
