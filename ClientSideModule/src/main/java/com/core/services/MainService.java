package com.core.services;

import com.core.dto.City;


import java.util.List;
import java.util.Map;

/**
 * Created by cihanblt on 6/13/2016.
 */
public interface MainService {


    public List<City> getAllCities();

    public boolean completeAllProcess();
}
