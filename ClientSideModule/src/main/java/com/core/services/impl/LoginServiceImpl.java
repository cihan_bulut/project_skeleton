package com.core.services.impl;

import com.guigarage.responsive.ResponsiveHandler;
import com.core.controller.AbstractService;
import com.core.controller.MainController;
import com.core.dto.UserInfo;
import com.core.services.LoginService;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by cihan-net-bt on 4/28/2016.
 */
public class LoginServiceImpl extends AbstractService implements LoginService {

    private final static Logger LOG = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Override
    public void setUserInfo(String s) {
        UserInfo userInfo = new UserInfo();
    }

    @Override
    public void doEntrance(ActionEvent actionEvent,UserInfo userInfo) {


        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxmls/main.fxml"));
            loader.load();
            Parent parent = loader.getRoot();
            Scene scene = new Scene(parent,1024,650);
            scene.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
            MainController mainController = loader.getController();
            mainController.setUserInfo(userInfo);
            
            Platform.runLater(new Runnable() {
                @Override
                public void run() {

                    Stage stage = new Stage();
                    stage.setScene(scene);
                    ResponsiveHandler.addResponsiveToWindow(stage);
//                    Image image = new Image("http://net-bt.com.tr/wp-content/uploads/2015/11/kantar.jpg");
//                    InputStream is = Thread.currentThread().getClass().getResourceAsStream("/images/logo.png");
                    Image image = new Image("http://goo.gl/kYEQl");
                    stage.setTitle("Kantar Client");
                    stage.getIcons().add(image);
                    stage.show();
//                    ((Node) actionEvent.getSource()).getScene().getWindow().hide();

                }
            });


        }catch (IOException e){
            LOG.error("main.fxml dont load");
            e.printStackTrace();
        }
    }

}
