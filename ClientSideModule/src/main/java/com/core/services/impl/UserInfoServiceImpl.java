package com.core.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.core.controller.AbstractService;
import com.core.dto.UserInfo;
import com.core.services.UserInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Asus on 27.07.2016.
 */
public class UserInfoServiceImpl extends AbstractService implements UserInfoService {

    private final static Logger LOG = LoggerFactory.getLogger(UserInfoServiceImpl.class);

    @Override
    public void updateUser(UserInfo user) {
        if(clientOperations.isAuthenticated()){
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String prod = objectMapper.writeValueAsString(user);
                clientOperations.postJsonData(prop.getProperty("userInfo.update.info"),prod);
            }catch (IOException e){
                LOG.error(e.getMessage());
            }
        }else{
            System.exit(0);
        }
    }

    @Override
    public Boolean checkPassword(UserInfo userInfo) {
        Boolean result = false;
        if(clientOperations.isAuthenticated()){
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String prod = objectMapper.writeValueAsString(userInfo);
                String d = clientOperations.postJsonData(prop.getProperty("userInfo.check.password"),prod);
                result = Boolean.valueOf(d);
            }catch (IOException e){
                LOG.error(e.getMessage());
            }
        }else{
            System.exit(0);
        }
        return result;
    }

    @Override
    public void changePassword(UserInfo user) {
        if(clientOperations.isAuthenticated()){
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String prod = objectMapper.writeValueAsString(user);
                String d = clientOperations.postJsonData(prop.getProperty("userInfo.change.password"),prod);
            }catch (IOException e){
                LOG.error(e.getMessage());
            }
        }else{
            System.exit(0);
        }
    }

}
