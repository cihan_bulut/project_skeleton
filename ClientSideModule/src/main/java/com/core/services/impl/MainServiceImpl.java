package com.core.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.core.controller.AbstractService;
import com.core.dto.City;
import com.core.services.MainService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cihanblt on 6/13/2016.
 */
public class MainServiceImpl extends AbstractService implements MainService {




    @Override
    public List<City> getAllCities() {
        if (clientOperations.isAuthenticated()) {
            List<City> cities = null;
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                String s = clientOperations.getContentBodyOnUrl(prop.getProperty("location.get.cities"));
                cities = objectMapper.readValue(s, objectMapper.getTypeFactory().constructCollectionType(List.class, City.class));
                return cities;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return null;
    }


    @Override
    public boolean completeAllProcess() {
        String ss = null;
        try {
           ss = clientOperations.getContentBodyOnUrl(prop.getProperty("location.complete.all.process"));
            System.out.println(ss);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
