package com.core.services;

import com.core.dto.UserInfo;
import javafx.event.ActionEvent;

/**
 * Created by cihan-net-bt on 4/28/2016.
 */
public interface LoginService {
    public void doEntrance(ActionEvent actionEvent, UserInfo userInfo);
    public void setUserInfo(String s);
}
