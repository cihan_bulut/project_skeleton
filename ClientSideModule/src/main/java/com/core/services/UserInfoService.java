package com.core.services;

import com.core.dto.UserInfo;

/**
 * Created by Asus on 27.07.2016.
 */
public interface UserInfoService {
    public void updateUser(UserInfo userInfo);
    public Boolean checkPassword(UserInfo userInfo);
    public void changePassword(UserInfo userInfo);
}
