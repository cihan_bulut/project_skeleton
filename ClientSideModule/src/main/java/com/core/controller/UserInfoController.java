package com.core.controller;

import com.core.dto.UserInfo;
import com.core.services.UserInfoService;
import com.core.services.impl.UserInfoServiceImpl;
import com.core.utils.ValidationUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Asus on 26.07.2016.
 */
public class UserInfoController extends AbstractService implements Initializable {
    @FXML
    TextField txtUserName;
    @FXML
    TextField txtName;
    @FXML
    TextField txtLastName;
    @FXML
    TextField txtEmail;
    @FXML
    Label lblError;
    @FXML
    Label lblError2;
    @FXML
    PasswordField txtOldPassword;
    @FXML
    PasswordField txtNewPassword;
    @FXML
    PasswordField txtNewPasswordAgain;
    private UserInfoService userInfoService = new UserInfoServiceImpl();

    private UserInfo userInfo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<TextField> list = new ArrayList<>();
        list.add(txtEmail);
        list.add(txtLastName);
        list.add(txtName);
        list.add(txtNewPassword);
        list.add(txtNewPasswordAgain);
        list.add(txtOldPassword);
//        list.add(txtUserName);
        ValidationUtils.setValidationTextfieldByList(list);
    }

    public void init() {
        lblError.setText("");
        lblError2.setText("");
    }

    @FXML
    public void btnChangeUserInfo() {
        ValidationUtils.removeValidationBorderTextField(txtName);
        ValidationUtils.removeValidationBorderTextField(txtLastName);
        ValidationUtils.removeValidationBorderTextField(txtEmail);
        lblError2.setText("");
        lblError.setText("");
        lblError.setTextFill(Color.RED);
        UserInfo u = new UserInfo();
        u.setUserName(userInfo.getUserName());
        if (txtName.getText() == null || txtName.getText().trim().isEmpty()) {
            ValidationUtils.addValidationBorderTextfield(txtName);
            lblError.setText("* Lütfen Adı Alanını Doldurunuz");
        } else if (txtLastName.getText() == null || txtLastName.getText().trim().isEmpty()) {
            ValidationUtils.addValidationBorderTextfield(txtLastName);
            lblError.setText("* Lütfen Soyadı Alanını Doldurunuz");
        } else if (txtEmail.getText() == null || txtEmail.getText().trim().isEmpty()) {
            ValidationUtils.addValidationBorderTextfield(txtEmail);
            lblError.setText("* Lütfen E-mail Alanını Doldurunuz");
        } else if (!txtEmail.getText().matches("(?simx)\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b")) {
            ValidationUtils.addValidationBorderTextfield(txtEmail);
            lblError.setText("* Geçersiz E-mail Adresi");
        } else {
            u.setFirstName(!txtName.getText().trim().isEmpty() ? txtName.getText() : userInfo.getFirstName());
            u.setLastName(!txtLastName.getText().trim().isEmpty() ? txtLastName.getText() : userInfo.getLastName());
            u.setEmail(!txtEmail.getText().trim().isEmpty() ? txtEmail.getText() : userInfo.getEmail());
            u.setAuthorities(userInfo.getAuthorities());
            u.setId(userInfo.getId());
            try {
                userInfoService.updateUser(u);
                lblError.setText("* Bilgileriniz Güncellendi");
                lblError.setTextFill(Color.GREEN);
            } catch (Exception ex) {
                lblError.setText(ex.toString());
                ex.printStackTrace();
            }
        }
    }

    @FXML
    public void checkTextfield(KeyEvent actionEvent) {
        TextField textField = (TextField) actionEvent.getSource();
        String nP = txtNewPasswordAgain.getText();
        String nPA = txtNewPassword.getText();

        if (textField.getText().trim().isEmpty()) {
            ValidationUtils.addValidationBorderTextfield(textField);
        } else ValidationUtils.removeValidationBorderTextField(textField);
//
//        if (textField == txtNewPasswordAgain && nP != null && nPA != null) {
//            if (nPA.equalsIgnoreCase(nP)) {
//                ValidationUtils.removeValidationBorderTextField(textField);
//            }else ValidationUtils.addValidationBorderTextfield(textField);
//        }
    }

    @FXML
    public void btnChangePassword() {
        ValidationUtils.removeValidationBorderTextField(txtOldPassword);
        ValidationUtils.removeValidationBorderTextField(txtNewPassword);
        ValidationUtils.removeValidationBorderTextField(txtNewPasswordAgain);
        lblError2.setTextFill(Color.RED);
        lblError2.setText("");
        lblError.setText("");
        UserInfo u = new UserInfo();
        u.setUserName(userInfo.getUserName());
        if (txtOldPassword.getText() == null || txtOldPassword.getText().trim().isEmpty()) {
            ValidationUtils.addValidationBorderTextfield(txtOldPassword);
            lblError2.setText("* Lütfen Şifreyi Giriniz");
        } else if (txtNewPassword.getText() == null || txtNewPassword.getText().trim().isEmpty()) {
            ValidationUtils.addValidationBorderTextfield(txtNewPassword);
            lblError2.setText("* Lütfen Yeni Şifreyi Giriniz");
        } else if (txtNewPasswordAgain.getText() == null || txtNewPasswordAgain.getText().trim().isEmpty()) {
            ValidationUtils.addValidationBorderTextfield(txtNewPasswordAgain);
            lblError2.setText("* Lütfen Yeni Şifreyi Tekrar Giriniz");
        } else if (!txtNewPassword.getText().equals(txtNewPasswordAgain.getText())) {
            ValidationUtils.addValidationBorderTextfield(txtNewPassword);
            ValidationUtils.addValidationBorderTextfield(txtNewPasswordAgain);
            lblError2.setText("* Şifreler Aynı Değil");
        } else {
            u.setPassword(txtOldPassword.getText());
            u.setId(userInfo.getId());
            if (userInfoService.checkPassword(u)) {
                try {
                    u.setPassword(txtNewPassword.getText());
                    userInfoService.changePassword(u);
                    lblError2.setText("* Şifreniz Değiştirildi");
                    lblError2.setTextFill(Color.GREEN);
                    txtOldPassword.clear();
                    txtNewPassword.clear();
                    txtNewPasswordAgain.clear();
                } catch (Exception ex) {
                    lblError2.setText(ex.toString());
                    ex.printStackTrace();
                }
            } else {
                ValidationUtils.addValidationBorderTextfield(txtOldPassword);
                lblError2.setText("* Şifreniz Hatalı");
            }
        }
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
        txtUserName.clear();
        txtName.clear();
        txtLastName.clear();
        txtEmail.clear();
        txtUserName.setText(userInfo.getUserName());
        txtName.setText(userInfo.getFirstName() == null ? " " : userInfo.getFirstName());
        txtLastName.setText(userInfo.getLastName() == null ? " " : userInfo.getLastName());
        txtEmail.setText(userInfo.getEmail() == null ? " " : userInfo.getEmail());
    }
}
