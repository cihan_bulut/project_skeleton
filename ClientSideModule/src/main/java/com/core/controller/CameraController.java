package com.core.controller;

import com.github.sarxos.webcam.Webcam;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;


/**
 * Created by cihanblt on 9/6/2016.
 */
public class CameraController implements Initializable {

    @FXML
    private ComboBox<String> chooseCam;
    private ObservableList<String> listCam = FXCollections.observableArrayList();
    @FXML
    private ImageView showImage;

    @FXML
    private Button takePlaque;

    private BufferedImage bufferedImage;

    @FXML
    private Label lblPlaquePromt;

    @FXML
    private ListView<String> listPlaque;
    protected ObservableList<String> listPlaqueData = FXCollections.observableArrayList();

    @FXML
    private ProgressIndicator progressIndicator;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        progressIndicator.setVisible(false);
        List<Webcam> webcams = Webcam.getWebcams();
        for (Webcam webcam : webcams){
            listCam.add(webcam.toString());
        }
        chooseCam.setItems(listCam);
        System.out.println(webcams.get(0) + " , " + webcams.get(1));

    }

    private int imageCount = 0;

    public void takePlaque(ActionEvent event) {

    }

    public void chooseListPlaque() {
        String plaque = listPlaque.getSelectionModel().getSelectedItem();
        lblPlaquePromt.setText(plaque);
    }

    public void takePicture() {
        listPlaqueData.clear();
        listPlaque.setItems(listPlaqueData);
        progressIndicator.setVisible(true);
        Task<Void> voidTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                takeAndProcess();
                return null;
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                progressIndicator.setVisible(false);
            }
        };
        progressIndicator.progressProperty().bind(voidTask.progressProperty());
        new Thread(voidTask).start();

    }

    public void takeAndProcess() {

        Webcam webcam = Webcam.getWebcams().get(chooseCam.getSelectionModel().getSelectedIndex());

        webcam.open();
        String path = "samples/" + imageCount + "-car-pic.png";
//        String path = "samples/car.png";
        File file = new File(path);
        try {
            bufferedImage = webcam.getImage();
            ImageIO.write(bufferedImage, "PNG", file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        webcam.close();
        try {
            InputStream inputStream = new FileInputStream(file);
            Image image = new Image(inputStream);
            showImage.setImage(image);
            processPicture(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        imageCount++;
    }

    public void processPicture(String licensePlate) throws Exception {
        String country = "eu", configfile = "openalpr.conf", runtimeDataDir = "runtime_data";

//        licensePlate = "samples/car2.PNG";

//        Alpr alpr = new Alpr(country, configfile, runtimeDataDir);
//
//        alpr.setTopN(10);
//        alpr.setDefaultRegion("wa");

        // Read an image into a byte array and send it to OpenALPR
        Path path = Paths.get(licensePlate); //CameraController.class.getResource(licensePlate).toURI()
        byte[] imagedata = Files.readAllBytes(path);

//        AlprResults results = alpr.recognize(imagedata);
//
//        System.out.println("OpenALPR Version: " + alpr.getVersion());
//        System.out.println("Image Size: " + results.getImgWidth() + "x" + results.getImgHeight());
//        System.out.println("Processing Time: " + results.getTotalProcessingTimeMs() + " ms");
//        System.out.println("Found " + results.getPlates().size() + " results");
//
//        System.out.format("  %-15s%-8s\n", "Plate Number", "Confidence");
//        for (AlprPlateResult result : results.getPlates()) {
//            for (AlprPlate plate : result.getTopNPlates()) {
//                if (plate.isMatchesTemplate())
//                    System.out.print("  * ");
//                else
//                    System.out.print("  - ");
//                Platform.runLater(new Runnable() {
//                    @Override
//                    public void run() {
//                        listPlaqueData.add(plate.getCharacters());
//                    }
//                });
//                System.out.format("%-15s%-8f\n", plate.getCharacters(), plate.getOverallConfidence());
//            }
//        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                lblPlaquePromt.setText("");
                if (listPlaqueData.size() > 0) {
                    listPlaque.setItems(listPlaqueData);
                    lblPlaquePromt.setText(listPlaqueData.get(0));
                } else {
                    lblPlaquePromt.setText("Bulunamadı..");
                }
            }
        });

        // Make sure to call this to release memory
//        alpr.unload();
    }

    public void includePicture(ActionEvent actionEvent) {
        listPlaqueData.clear();
        listPlaque.setItems(listPlaqueData);
        Scene scene = ((Node) actionEvent.getSource()).getScene();
        Stage stage = (Stage) scene.getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Resim Yükle");
        File file = fileChooser.showOpenDialog(stage);
        if (file != null && file.exists()) {
            try {
                progressIndicator.setVisible(true);
                Task<Void> voidTask = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        showImage.setImage(new Image(new FileInputStream(file)));
                        processPicture(file.getAbsolutePath());
                        return null;
                    }

                    @Override
                    protected void succeeded() {
                        super.succeeded();
                        progressIndicator.setVisible(false);
                    }
                };
                progressIndicator.progressProperty().bind(voidTask.progressProperty());
                new Thread(voidTask).start();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    public static void main(String[] args) {
//        System.out.println(System.getProperty("java.library.path"));
////        System.loadLibrary("openalprjni");
//        try {
//            processPicture();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

}
