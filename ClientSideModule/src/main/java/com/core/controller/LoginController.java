package com.core.controller;

import com.core.dto.UserInfo;
import com.core.services.LoginService;
import com.core.services.impl.LoginServiceImpl;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import okhttp3.Credentials;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by cihan-blt on 7.04.2016.
 */
public class LoginController extends AbstractService implements Initializable {

    private LoginService loginService = new LoginServiceImpl();
    @FXML
    private TextField txtUserName;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Label lblMessage;

    @FXML
    private Button btnLogin;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txtUserName.requestFocus();
        txtUserName.setOnKeyPressed(pressEnterKey());
        txtPassword.setOnKeyPressed(pressEnterKey());

    }

    @FXML
    private void btnLoginAction(ActionEvent actionEvent) throws IOException {
        doLogin(actionEvent);


    }

    private ActionEvent actionEvent;

    public void doLogin(ActionEvent actionEvent) throws IOException {
        this.actionEvent = actionEvent;

//        mythread2 mythread2 = new mythread2();
//        mythread2.start();
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                login();
                return null;
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        openLoadingWindow(task);


    }

    public EventHandler<KeyEvent> pressEnterKey(){
        return new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode().equals(KeyCode.ENTER)){
                    try{
                        doLogin(actionEvent);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    public void login() throws IOException {


        String credential = Credentials.basic(txtUserName.getText(), txtPassword.getText());
        String path = prop.getProperty("user.login");
        if (clientOperations.authenticate(path, credential)) {
            Platform.runLater(()->{
                Stage stage =(Stage)btnLogin.getScene().getWindow();
                stage.close();
            });
            UserInfo user = clientOperations.getUserInfo();
            loginService.doEntrance(actionEvent, user);
            closeLoadingWindow();
        } else {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    lblMessage.setText("Kullanıcı adı veya şifreniz yanlış!!");
                    closeLoadingWindow();
                }
            });
        }

    }

    private class mythread2 extends Thread {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            super.run();
            try {
                login();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
    }
}
