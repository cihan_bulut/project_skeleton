package com.core.controller;

import com.core.httpactions.ClientOperations;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Created by cihan-blt on 7.04.2016.
 */
public abstract class AbstractService {

    protected static boolean isDone;

    protected final Logger LOG = LoggerFactory.getLogger(getClass());

    //        protected static final String SERVER_URL = "http://192.168.1.77:8080";
    protected static final String SERVER_URL = "http://localhost:8080";
//    protected static final String SERVER_URL = "http://192.168.1.40:8080";
//    protected static final String SERVER_URL = "http://192.168.3.103:8080/scales";

    protected static final ClientOperations clientOperations = ClientOperations.getInstance();

    private ResourceBundle resourceBundle = ResourceBundle.getBundle("urls");


    protected static Properties prop = new Properties();

    {
        Set<String> keys = resourceBundle.keySet();

        Iterator<String> keysIterator = keys.iterator();

        while (keysIterator.hasNext()) {
            String key = keysIterator.next();
            prop.put(key, SERVER_URL + resourceBundle.getObject(key));
        }
    }

    public Stage openWindow(ActionEvent actionEvent, String fxmlFile, MainController mainController, String type) {
        return null;
    }

    public void init(TextField[] textField) {
        for (TextField field : textField) {
            field.setDisable(true);
        }

    }

    private Stage stage;

    public void openLoadingWindow(Task task) {

        stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setAlwaysOnTop(true);
        stage.resizableProperty().set(false);

        StackPane stackPane = new StackPane();
        Image image = new Image("/images/spin.gif");

        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(40);
        imageView.setFitHeight(40);
        Label label = new Label("Yükleniyor...");
        label.setGraphic(imageView);
        label.setPrefWidth(150);
        label.setPrefHeight(80);
//        ProgressIndicator progressIndicator = new ProgressIndicator();
//        ProgressBar progressIndicator = new ProgressBar(0);

//        progressIndicator.progressProperty().bind(task.progressProperty());

        stackPane.getChildren().addAll(label);
        Scene scene = new Scene(stackPane, 300, 60);
        stage.setScene(scene);
        stage.show();

    }

    public void closeLoadingWindow() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                if (stage != null)
                    stage.close();
            }
        });
    }
}
