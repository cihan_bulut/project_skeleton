package com.core.utils;

/**
 * @author Faruk
 */

import com.core.dto.EventTypes;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import jxl.Workbook;
import jxl.write.*;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExcelExporter {
    public ExcelExporter() {
    }

    public static void fillData(TableView table, EventTypes type) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XLS File (*.xls)", "*.xls");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(null);
        if (file != null)
            try {
                WritableWorkbook workbook1 = Workbook.createWorkbook(new File(file.toString()));
                WritableSheet sheet1 = workbook1.createSheet("First Sheet", 0);
                WritableFont cellFont = new WritableFont(WritableFont.ARIAL, 9);
                cellFont.setBoldStyle(WritableFont.BOLD);
                WritableCellFormat cellFormat = new WritableCellFormat(cellFont);

                for (int i = 0; i < table.getColumns().size(); i++) {
                    TableColumn tableColumn = (TableColumn) table.getColumns().get(i);
                    Label column = new Label(i, 0, tableColumn.getText(),cellFormat);
                    sheet1.addCell(column);
                }

                workbook1.write();
                workbook1.close();
                infoBox("Kaydedildi", Alert.AlertType.INFORMATION);
            } catch (Exception ex) {
                infoBox("Hata Oluştu", Alert.AlertType.ERROR);
                Logger.getLogger(ExcelExporter.class.getName()).log(Level.SEVERE, "Unable to Export Data", ex);
            }
        else infoBox("İptal Edildi!", Alert.AlertType.WARNING);
    }

    public static void infoBox(String message, Alert.AlertType alertType) {
        Alert alert = null;
        if (Alert.AlertType.INFORMATION == alertType) {
            alert = new Alert(alertType);
            alert.setTitle("Bilgi");
        } else if (Alert.AlertType.ERROR == alertType) {
            alert = new Alert(alertType);
            alert.setTitle("Hata");
        } else if (Alert.AlertType.WARNING == alertType) {
            alert = new Alert(alertType);
            alert.setTitle("Uyarı");
        }
        alert.setHeaderText("");
        alert.setContentText(message);
        alert.showAndWait();
    }
}
