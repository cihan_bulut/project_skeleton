package com.core.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Created by Asus on 19.07.2016.
 */
public class MessageUtils {
    public static void showErrorMessage(String context) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        Button okButton = (Button) alert.getDialogPane().lookupButton( ButtonType.OK );
        okButton.setText("Tamam");
        alert.setTitle("Hata");
        alert.setHeaderText(null);
        alert.setContentText(context);
        alert.showAndWait();

    }
    public static void showInformationMessage(String context){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        Button okButton = (Button) alert.getDialogPane().lookupButton( ButtonType.OK );
        okButton.setText("Tamam");
        alert.setTitle("Bilgi");
        alert.setHeaderText(null);
        alert.setContentText(context);
        alert.showAndWait();
    }

    public static void showWarningMessage(String context){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        Button okButton = (Button) alert.getDialogPane().lookupButton( ButtonType.OK );
        okButton.setText("Tamam");
        alert.setTitle("Uyarı");
        alert.setHeaderText(null);
        alert.setContentText(context);
        alert.showAndWait();
    }

    public static Optional<ButtonType> showConfirmMessage(String context){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        Button okButton = (Button) alert.getDialogPane().lookupButton( ButtonType.OK );
        okButton.setText("Evet");
        Button cancelButton = (Button) alert.getDialogPane().lookupButton( ButtonType.CANCEL);
        cancelButton.setText("Hayır");
        alert.setTitle("Onay");
        alert.setHeaderText(null);
        alert.setContentText(context);
        return alert.showAndWait();
    }
}
