package com.core.utils;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

import java.util.List;

/**
 * Created by Asus on 5.08.2016.
 */
public class ValidationUtils {
    public static void setValidationTextfieldByList(List<TextField> tfs) {
        ValidationSupport validationSupport = new ValidationSupport();
        Platform.runLater(() -> {
            for (TextField tf : tfs) {
                validationSupport.registerValidator(tf, Validator.createEmptyValidator("Bu Alan Gerekli"));
                ValidationSupport.setRequired(tf, true);
            }
        });
    }

    public static void setValidationTextfield(TextField tf) {
        ValidationSupport validationSupport = new ValidationSupport();
        validationSupport.registerValidator(tf, Validator.createEmptyValidator("Bu Alan Gerekli"));
        ValidationSupport.setRequired(tf, true);

    }

    private static void setValidationCombobox(ComboBox cb) {
        ValidationSupport validationSupport = new ValidationSupport();
        validationSupport.registerValidator(cb, Validator.createEmptyValidator("Bu Alan Gerekli"));
        ValidationSupport.setRequired(cb, true);

    }

    private static void setValidationRadioButton(RadioButton rb) {
        ValidationSupport validationSupport = new ValidationSupport();
        validationSupport.registerValidator(rb, Validator.createEmptyValidator("Bu Alan Gerekli"));
        ValidationSupport.setRequired(rb, true);

    }

    public static void setValidationTextArea(TextArea ta) {
        ValidationSupport validationSupport = new ValidationSupport();
        validationSupport.registerValidator(ta, Validator.createEmptyValidator("Bu Alan Gerekli"));
        ValidationSupport.setRequired(ta, true);

    }

    public static void numericValidationTextfield(TextField textField, Integer max_Lenght) {
        textField.addEventFilter(KeyEvent.KEY_TYPED, numeric_Validation(max_Lenght));
    }

    public static void addValidationBorderTextfield(TextField tf) {
        tf.getStyleClass().addAll("error");
        ValidationSupport.setRequired(tf, true);
    }

    public static void removeValidationBorderTextField(TextField tf) {
        tf.getStyleClass().remove("error");
        ValidationSupport.setRequired(tf, false);
    }

    public static void addValidationBorderTextArea(TextArea ta) {
        ta.getStyleClass().addAll("error");
        ValidationSupport.setRequired(ta, true);
    }

    public static void removeValidationBorderTextArea(TextArea ta) {
        ta.getStyleClass().remove("error");
        ValidationSupport.setRequired(ta, false);
    }

    public static void addValidationBorderCombobox(ComboBox cb) {
        cb.getStyleClass().addAll("error");
        ValidationSupport.setRequired(cb, true);
    }

    public static void removeValidationBorderCombobox(ComboBox cb) {
        cb.getStyleClass().remove("error");
        ValidationSupport.setRequired(cb, false);
    }

    public static void addValidationBorderRadioButton(RadioButton rb) {
        rb.getStyleClass().addAll("error");
        ValidationSupport.setRequired(rb, true);
    }

    public static void removeValidationBorderRadioButton(RadioButton cb) {
        cb.getStyleClass().remove("error");
        ValidationSupport.setRequired(cb, false);
    }

    private static EventHandler<KeyEvent> numeric_Validation(final Integer max_Lengh) {
        return new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent e) {
                TextField txt_TextField = (TextField) e.getSource();
                if (txt_TextField.getText().length() >= max_Lengh) {
                    e.consume();
                }
                if (e.getCharacter().matches("[0-9.]")) {
                    if (txt_TextField.getText().contains(".") && e.getCharacter().matches("[.]")) {
                        e.consume();
                    } else if (txt_TextField.getText().length() == 0 && e.getCharacter().matches("[.]")) {
                        e.consume();
                    }
                } else {
                    e.consume();
                }
            }
        };
    }
}
