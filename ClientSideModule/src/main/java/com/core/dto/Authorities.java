package com.core.dto;

/**
 * Created by cihanblt on 6/8/2016.
 */
public class Authorities {

    private String authority;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
