package com.core.models;

import javax.persistence.*;

/**
 * Created by cihan-net-bt on 4/26/2016.
 */
@Entity
@Table(name = "TOKEN_INFO")
public class TokenInfo extends AbstractPersistable{

    @Column
    private String token;

    @Column(name = "user_name")
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
