package com.core.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * Created by cihan-net-bt on 5.04.2016.
 */
@MappedSuperclass
public abstract class AbstractPersistable implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "record_id")
    private Long id;

    @Column(name = "version")
    @Version
    @JsonIgnore
    private Long version;

    @Column(name = "is_active")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    @JsonIgnore
    private boolean isActive = true;

    @Column(name = "creation_id")
    @JsonIgnore
    private Long creationId;

    @Column(name = "last_updated_id")
    @JsonIgnore
    private Long lastUpdatedId;

    @Temporal(TemporalType.DATE)
    @Column(name = "creation_date")
    @JsonIgnore
    private Date creationDate = new Date();

    @Temporal(TemporalType.DATE)
    @Column(name = "last_updated_date")
    @JsonIgnore
    private Date lastUpdatedDate;

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getCreationId() {
        return creationId;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationId(Long creationId) {
        this.creationId = creationId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Long getLastUpdatedId() {
        return lastUpdatedId;
    }

    public void setLastUpdatedId(Long lastUpdatedId) {
        this.lastUpdatedId = lastUpdatedId;
    }


}
