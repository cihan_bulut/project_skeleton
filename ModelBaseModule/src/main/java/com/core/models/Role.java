package com.core.models;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by cihan-net-bt on 5.04.2016.
 */
@Entity
@Table(name = "ROLES")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Role extends AbstractPersistable{
    @Column
    private String name;
    @Column
    private String description;


    @ManyToMany(mappedBy = "roles")
    @JsonIgnore
    private Set<User> users;

    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(
            name = "roles_rights",
            joinColumns = @JoinColumn(name = "role_id",referencedColumnName = "record_id"),
            inverseJoinColumns = @JoinColumn(name = "right_id",referencedColumnName = "record_id"))
    private Set<Right> rights;

    public Set<Right> getRights() {
        return rights;
    }

    public void setRights(Set<Right> rights) {
        this.rights = rights;
    }

    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Role{" +
                "description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", users=" + users +
                ", rights=" + rights +
                '}';
    }
}
