// app.js
var myApp = angular.module('yapp',[]);

myApp.run(function ($rootScope,$state) {
  //
  // $rootScope.$on('$locationChangeSuccess', function() {
  //       // log-in promise failed. Redirect to log-in page.
  //       console.log('ssss');
  //       $state.go('dashboard');
  // });

});

myApp.factory('requestFactory', ['$http', function($http,sessionService){
  requestFactory.postData = function (url,data) {
    if(sessionService.get('auth') != ''){
      return $http.post(url, {
        data : data,
        headers: {
          'Authorization': "Basic " + sessionService.get('auth'),
          'content-type': 'application/json',
          'X-Auth-Token': sessionService.get('token')
        }
      });
    }
  };

  requestFactory.getData = function (url) {
    if(sessionService.get('auth') != ''){
      return $http.post(url, {
        headers: {
          'Authorization': "Basic " + sessionService.get('auth'),
          'content-type': 'application/json',
          'X-Auth-Token': sessionService.get('token')
        }
      });
    }
  };
}]);
