"use strict";
angular.module("yapp", ["ui.router", "ngAnimate", "ngDialog", 'objectTable']).config(["$stateProvider", "$urlRouterProvider", function (r, t) {
  // t.when("/dashboard", "/dashboard/user", "/dashboard/add_edit_user.html")
  t.otherwise("/login"), r.state("base", {
    "abstract": !0,
    url: "",
    templateUrl: "views/base.html",
  }).state("login", {
    url: "/login",
    parent: "base",
    templateUrl: "views/login.html",
    controller: "LoginCtrl",
  }).state("dashboard", {
    url: "/dashboard",
    parent: "base",
    templateUrl: "views/dashboard.html",
    controller: "DashboardCtrl"
  }).state("user", {
    url: "/user",
    parent: "dashboard",
    templateUrl: "views/dashboard/user_process.html",
    authenticated: true
  }).state("location", {
    url: "/location",
    parent: "dashboard",
    templateUrl: "views/dashboard/location_process.html",
    authenticated: true
  }).state("firm", {
    url: "/firm",
    parent: "dashboard",
    templateUrl: "views/dashboard/firm_process.html",
    authenticated: true
  }).state("product", {
    url: "/product",
    parent: "dashboard",
    templateUrl: "views/dashboard/product_process.html",
    authenticated: true
  });
}]);
/*
 , angular.module("yapp").controller("LoginCtrl", ["$scope", "$location", function (r, t) {
 r.submit = function () {
 return t.path("/dashboard"), !1
 }
 }]), angular.module("yapp").controller("DashboardCtrl", ["$scope", "$state", function (r, t) {
 r.$state = t
 }]);
 */
