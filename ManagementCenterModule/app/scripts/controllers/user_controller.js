/**
 * Created by Asus on 15.08.2016.
 */
  angular.module("yapp").controller("UserController", ["$scope", 'ngDialog', '$http', function ($scope, ngDialog, $http,requestFactory) {
    var url = "http://192.168.1.45:8080";
    $http.get(url + '/login/get-all-users', {
      headers: {
        'Authorization': "Basic " + sessionStorage.getItem('auth'),
        'content-type': 'application/json',
        'X-Auth-Token': sessionStorage.getItem('token')
      }
    }).success(function (data, status, headers, config) {
      $scope.userData = data;
    }).error(function (data, error, status, headers, config) {
      console.log(error);
    });

    // $scope.userData = requestFactory.getData('http://localhost:8080/login/get-all-users');


    $scope.report = {
      selectedPerson: null
    };

    $scope.test = function (e) {
      alert('Alert from controller method!');
    };

    $scope.showItem = function (item) {
      alert(JSON.stringify(item));
    };

    $scope.getTotalBalance = function (data) {
      //return if empty or not ready
      if (!data || !data.length) return;

      var totalNumber = 0;
      for (var i = 0; i < data.length; i++) {
        totalNumber = totalNumber + parseFloat(data[i].money);
      }

      return Math.round(totalNumber);

    };

    // console.log($scope.getData("http://localhost:8080/login/user-info"));

    // this variable will contains all data after loading
    $scope.dataFromUrl = [];

    /*codemirror*/

    $scope.editorOptions = {
      lineNumbers: true,
      readOnly: 'nocursor'
    };

    $scope.editorOptionsJS = {
      lineNumbers: true,
      readOnly: 'nocursor',
      mode: "javascript"
    };

    $scope.clickToOpen = function (selectedItem) {
      // $scope.selectedClass = $scope.data[index];
      selectedItem.passwordConfirm = selectedItem.password;
      $scope.user = selectedItem;
      ngDialog.open({

        template: '../views/dashboard/add_edit_user.html',
        className: 'ngdialog-theme-default',
        controller: 'PopupController',
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        scope: $scope
      });
    };//end clickToOpen

    $scope.clickToOpenEmptyUser = function () {
      // $scope.user = null;
      ngDialog.open({
        template: '../views/dashboard/add_edit_user.html',
        className: 'ngdialog-theme-default',
        controller: 'PopupController',
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        scope: $scope
      });
    };//end clickToOpenEmpty

    $scope.deleteUser = function (selectedItem) {
      ngDialog.openConfirm({
        template: 'modalDialogId',
        className: 'ngdialog-theme-plain'

      }).then(function (value) {
        $scope.deletedUser = selectedItem;
        $scope.data.splice(1, 1);
        if (!$scope.$$phase) {
          $scope.$apply();
          //$digest or $apply
        }

        // $scope.user.splice(index, 1);
        // delete user[deletedUser];
        // $scope.userProfile.users.splice(index, 1);
        console.log('Modal promise resolved. Value: ', value);
      }, function (reason) {
        console.log('Modal promise rejected. Reason: ', reason);
      });
    };

    //end deleteUser
  }]);

