/**
 * Created by Asus on 15.08.2016.
 */
(function () {
  angular.module("yapp").controller("FirmController", ["$scope", 'ngDialog', function ($scope, ngDialog) {
    $scope.userProfile = {
      users: [
        {
          id: 1,
          userName: 'Physics',
          firstName: 70,
          lastName: "asdasas",
          nationalId: "156161",
          phoneNumber: "312312",
          email: "mehmet@gmail.com",
          password: 'a12345',
          passwordConfirm: 'a12345'
        },
        {
          id: 2,
          userName: 'Chemistry',
          firstName: 80,
          lastName: "asdasas",
          nationalId: "156161",
          phoneNumber: "312312",
          email: "mehmet@gmail.com",
          password: 'a12345',
          passwordConfirm: 'a12345'
        },
        {
          id: 3,
          userName: 'Math',
          firstName: 65,
          lastName: "asdasas",
          nationalId: "156161",
          phoneNumber: "312312",
          email: "mehmet@gmail.com",
          password: 'a12345',
          passwordConfirm: 'a12345'
        },
        {
          id: 4,
          userName: 'English',
          firstName: 75,
          lastName: "asdasas",
          nationalId: "156161",
          phoneNumber: "312312",
          email: "mehmet@gmail.com",
          password: 'a12345',
          passwordConfirm: 'a12345'
        },
        {
          id: 5,
          userName: 'Hindi',
          firstName: 67,
          lastName: "asdasas",
          nationalId: "156161",
          phoneNumber: "312312",
          email: "mehmet@gmail.com",
          password: 'a12345',
          passwordConfirm: 'a12345'
        }
      ]
    };

    $scope.clickToOpen = function (index) {
      $scope.selectedClass = $scope.userProfile.users[index];
      $scope.firm = $scope.userProfile.users[index];
      ngDialog.open({
        template: '../views/dashboard/add_edit_firm.html',
        className: 'ngdialog-theme-default',
        controller: 'PopupController',
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        scope: $scope
      });
    };//end clickToOpen

    $scope.clickToOpenEmpty = function () {
      $scope.user = null;
      ngDialog.open({
        template: '../views/dashboard/add_edit_firm.html',
        className: 'ngdialog-theme-default',
        controller: 'PopupController',
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        scope: $scope
      });
    };//end clickToOpenEmpty

    $scope.deleteUser = function (index) {

      ngDialog.openConfirm({
        template: 'modalDialogId',
        className: 'ngdialog-theme-plain'

      }).then(function (value) {
        $scope.deletedUser = $scope.userProfile.users[index];
        $scope.userProfile.users.splice(index, 1);
        console.log('Modal promise resolved. Value: ', value);
      }, function (reason) {
        console.log('Modal promise rejected. Reason: ', reason);
      });
    };

    //end deleteUser
  }]);


})();
