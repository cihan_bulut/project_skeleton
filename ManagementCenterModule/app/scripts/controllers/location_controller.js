/**
 * Created by Asus on 15.08.2016.
 */
(function () {
  angular.module("yapp").controller("LocationController", ["$scope", 'ngDialog', function ($scope, ngDialog) {
    // $scope.clickToOpen = 'clickToOpen';
    $scope.locationProfile = {
      locations: [
        {
          id: 1,
          locationName: 'denizli',
          place: 70,
          scalesName: "kantar1",
          scalesDesc: "xxx kantar",
          city:'denizli'
        }
      ]
    };

    $scope.clickToOpen = function (index) {
      $scope.selectedClass = $scope.userProfile.users[index];
      $scope.user = $scope.userProfile.users[index];
      ngDialog.open({
        template: '../views/dashboard/add_edit_user.html',
        className: 'ngdialog-theme-default',
        controller: 'PopupController',
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        scope: $scope
      });
    };//end clickToOpen

    $scope.clickToOpenEmpty = function () {
      $scope.user = null;
      ngDialog.open({
        template: '../views/dashboard/add_edit_user.html',
        className: 'ngdialog-theme-default',
        controller: 'PopupController',
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        scope: $scope
      });
    };//end clickToOpenEmpty

    $scope.clickToOpenEmptyLocation = function () {
      $scope.user = null;
      ngDialog.open({
        template: '../views/dashboard/add_edit_location.html',
        className: 'ngdialog-theme-default',
        controller: 'PopupController',
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        scope: $scope
      });
    };

    $scope.deleteUser = function (index) {
      ngDialog.openConfirm({
        template: 'modalDialogId',
        className: 'ngdialog-theme-plain'

      }).then(function (value) {
        $scope.deletedUser = $scope.userProfile.users[index];
        $scope.userProfile.users.splice(index, 1);
        console.log('Modal promise resolved. Value: ', value);
      }, function (reason) {
        console.log('Modal promise rejected. Reason: ', reason);
      });
    };

    //end deleteUser
  }]);


})();
