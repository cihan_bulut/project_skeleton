'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */

  angular.module('yapp').controller('LoginCtrl', function ($rootScope,$scope, $location, $http,sessionService) {
    var token = null;
    var url = "http://192.168.1.45:8080";
    $scope.userData = null;
    $scope.submit = function () {
      var authdata = btoa($scope.username + ":" + $scope.password);
      console.log(authdata)
      $http.get(url + '/login/user-info', {
        headers: {
          'Authorization': "Basic " + authdata,
          'Content-Type': 'application/json'
        }
      }).success(function (data, status, headers, config) {
        console.log("data : " + data);
      }).error(function (data, status, headers, config) {
        token = headers('X-Auth-Token');
        console.log("token : " + token);
        var promise = getUserInfo(authdata, token)
        promise.success(function (data, status, headers, config) {
          $scope.userData = data;
          console.log("data : " + data);
          console.log("authentication is ok" + $scope.userData['userName']);
          sessionService.set('auth',authdata);
          sessionService.set('token',token);
          $location.path('/dashboard/user');
        });
        promise.error(function (data, status, headers, config) {
          $scope.errorMessage = "Kullanıcı adı veya şifreniz yanlış!!";
          console.log("result : " + status);
        });
      });

    };
    var getUserInfo = function (authdata, tokenn) {
      console.log(tokenn);
      return $http.get(url + '/login/user-info', {
        headers: {
          'Authorization': "Basic " + authdata,
          'content-type': 'application/json',
          'X-Auth-Token': tokenn
        }
      });
    };



  });
myApp.config(['$httpProvider', function ($httpProvider) {
  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);
